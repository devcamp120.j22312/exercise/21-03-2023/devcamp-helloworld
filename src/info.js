const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: "https://cdn1.iconfinder.com/data/icons/programing-development-8/24/react_logo-1024.png",
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  }

  function tinhTyLeSinhVienDangHoc() {
    return gDevcampReact.studyingStudents / gDevcampReact.totalStudents * 100;
  }

  var sPercentStudyingStudents = tinhTyLeSinhVienDangHoc();

  export {gDevcampReact, tinhTyLeSinhVienDangHoc, sPercentStudyingStudents}