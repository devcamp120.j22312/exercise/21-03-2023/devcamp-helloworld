import {gDevcampReact, tinhTyLeSinhVienDangHoc, sPercentStudyingStudents} from "./info"

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} alt="image" width="400"></img>
      <p>Tỷ lệ sinh viên đang theo học: {sPercentStudyingStudents} %</p>
      <p>{sPercentStudyingStudents > 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít"}</p>
      <ul>
        {gDevcampReact.benefits.map(function(element, index) {
          return <li key={index}>{element}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
